(function ($, Drupal) {
  Drupal.behaviors.FieldgroupScrollableTabs = {
    attach: function (context, settings) {
      var itemsSettings = Drupal.settings.field_group_scrollable_tabs.items_settings;

      /**
       * Determinates if the item is the first which the user can access.
       *
       * @param {element} elem
       *   This item will be tested.
       * @returns {boolean}
       *   Returns true if the item is the first visible item.
       */
      function isFirstItem(elem) {
        var parent = elem.parent();

        return parent.children(":visible:first").is(elem);
      }

      /**
       * Determinates if the item is the last which the user can access.
       *
       * @param {element} elem
       *   This item will be tested.
       * @returns {boolean}
       *   Returns true if the item is the last visible item.
       */
      function isLastItem(elem) {
        var parent = elem.parent();

        return parent.children(":visible:last").is(elem);
      }

      /**
       * Determinate if an item is active or not.
       *
       * @param {element} elem
       *   This item will be tested.
       * @param {int} virtualActiveLine
       *   For single selection the activation trashhold.
       * @param {boolean} singleActive bool
       *   Only one item can be active or not.
       * @returns {boolean}
       *   Returns true if the item should be active.
       */
      function isActiveItem(elem, virtualActiveLine, singleActive)
      {
        var docViewTop = $(window).scrollTop() || 0;
        var docViewBottom = docViewTop + $(window).height() || 0;

        var elemTopOffset = elem.offset().top;
        var elemBottomOffset = elemTopOffset + elem.outerHeight(true);

        if (singleActive) {
          // Only single menu item can be active in the same time.
          // 1. row: if the first item doesn't reached the active line, still
          // should be active.
          // 2. row: general case, it the content intersects with the active
          // line, than it's true.
          // 3. row: if we reach the bottom of the page, the last item should be
          // selected.
          return (
            isFirstItem(elem) && elemTopOffset > docViewTop ||
            docViewBottom != $(document).height() && elemTopOffset - docViewTop <= virtualActiveLine && elemBottomOffset - docViewTop > virtualActiveLine ||
            isLastItem(elem) && docViewBottom == $(document).height()
          );
        }

        return elemTopOffset < docViewBottom && elemBottomOffset > docViewTop;
      }

      /**
       * This function collapse or expand an tab item.
       *
       * @param {element} tabItem
       *   Triggered tab item.
       */
      function collapseExpand(tabItem) {
        if (itemsSettings[tabItem.data('identifier')].collapse_animation) {
          // Animated collapsion.
          tabItem.toggleClass("collapsed-animated");
          if (!tabItem.hasClass("collapsed-animated")) {
            tabItem.find(".tab-content").slideDown(itemsSettings[tabItem.data('identifier')].collapsing_duration);
          }
          else {
            tabItem.find(".tab-content").slideUp(itemsSettings[tabItem.data('identifier')].collapsing_duration);
          }
        }
        else {
          // Simple collapsion via CSS.
          tabItem.toggleClass("collapsed");
        }
      }

      $(Drupal.settings.field_group_scrollable_tabs.items).each(function(key, value){
        $("#" + value.identifier).once('field-group-scrollable-tabs', function () {
          // For collapse and expand.
          $(this).find(".collapsible .tab-title").click(function () {
            collapseExpand($(this).parent(".tab-item"));
            $(window).scroll();
          });

          // For menu to fix the position.
          var menuList = $(this).find(".scrollable-tabs-list"),
            stop      = menuList.offset().top - value.fixed_menu_pos;

          $(window).scroll(function (e) {
            var scrollTop = $(window).scrollTop() || 0;

            if (scrollTop >= stop) {
              // Stick the div.
              menuList.addClass('fixed');
            } else {
              // Release the div.
              menuList.removeClass('fixed');
            }
          });

          // Add scroll function on click to menu items.
          menuList.find("li").click(function (e) {
            var gotoClass = $(this).data("tab-identifier");
            var tabItem = $(this).parents("#" + value.identifier).find("[data-identifier='" + gotoClass + "']");

            $('html, body').animate({
              'scrollTop' : Math.round(tabItem.offset().top - value.top_scroll_offset + 0.5)
            });

            // If it's collapsed, we expand it.
            if (tabItem.hasClass('collapsed-animated') || tabItem.hasClass('collapsed')) {
              collapseExpand(tabItem);
            }
          });

          // Add active menu item indicator.
          $(this).find("[data-identifier]").once("menu-indicator", function (e) {
            var contentContainer = $(this);
            $(window).scroll(function (e) {
              var identifier = contentContainer.data("identifier");
              if (isActiveItem(contentContainer, value.top_scroll_offset, value.single_active_only)) {
                contentContainer.parents("#" + value.identifier).find("[data-tab-identifier='" + identifier + "']").addClass("active");
              }
              else {
                contentContainer.parents("#" + value.identifier).find("[data-tab-identifier='" + identifier + "']").removeClass("active");
              }
            });
          });

          // Trigger scroll to set everything to the correct state after reload.
          $(window).scroll();
        });
      });
    }
  };
})(jQuery, Drupal);