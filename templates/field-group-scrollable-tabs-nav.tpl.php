<?php
/**
 * @file
 * Field Group: Easy Responsive Tabs to Accordion.
 *
 * Available variables:
 * - $is_empty: Boolean: any content to render at all?
 * - $wrapper_classes: The classes set on the wrapping div.
 * - $nav_classes: The classes set on the nav ul.
 * - $navs: An array of nav elements
 * - $pane_classes: The classes set on the panes containing content.
 * - $panes: An array of panes containing content.
 *
 * @ingroup themeable
 */
?>

<?php if (!$is_empty) : ?>
  <div class="<?php print $wrapper_classes; ?>">
    <div id="<?php print $identifier; ?>" class="<?php print $identifier; ?> type-<?php print $type; ?>">
      <ul class="scrollable-tabs-list <?php print $nav_classes; ?>">
        <?php
        foreach ($navs as $index => $nav) : ?>
          <li data-tab-identifier="<?php print $nav['tab-identifier']; ?>" class="scrollable-tabs-list-item <?php echo $index == 0 ? "active" : ""; ?>"><?php print $nav['content']; ?></li>
        <?php endforeach; ?>
      </ul>

      <div class="scrollable-tabs-container <?php print $pane_classes; ?>">
        <?php foreach ($panes as $index => $pane) : ?>
          <div class="tab-item <?php print $pane['class']; ?>" data-identifier="<?php print $pane['tab-identifier']; ?>">
            <div class="tab-title"><?php print $pane['title']; ?></div>
            <div class="tab-content"><?php print $pane['content']; ?></div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>

    <div class="clear clearfix"></div>
  </div>
<?php endif; ?>
